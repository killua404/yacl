section .bss
    ;variables
    num1 resb 4
    num2 resb 4
    resu resb 4

section .data
    ;constants
    myText: db "Hello World!", 10   ;string to print
    myTextLen: equ $-myText         ;length of string

section .text
    global _start

    _start:

    mov rbx, [num1]
    mov rbp, [num2]
    mov rbx, 10
    mov rbp, 20
    add rbx, rbp
    mov [resu], rbp


    mov rax, 1          ;call to sysout
    mov rdi, 1          ; no file??
    mov rsi, resu       ;result
    mov rdx, 4          ;text len
    syscall

    mov rax, 60         ;exit
    mov rdi, 0          ;return code 0
    syscall