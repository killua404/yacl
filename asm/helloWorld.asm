section .bss
    ;variables

section .data
    ;constants
    myText: db "Hello World!", 10   ;string to print
    myTextLen: equ $-myText         ;length of string

section .text
    global _start

    _start:
    mov rax, 1           ;call to sysout
    mov rdi, 1           ; no file??
    mov rsi, myText      ;text
    mov rdx, myTextLen   ;text len
    syscall

    mov rax, 60         ;exit
    mov rdi, 0          ;return code 0
    syscall